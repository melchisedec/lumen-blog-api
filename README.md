# lumen blog api

Find the endpoints in the readme.md file

GET all posts
http://localhost:8000/api/v1/posts/

GET one post
http://localhost:8000/api/v1/posts/view/1

POST : add a post
http://localhost:8000/api/v1/posts/add?title=last Test&body=Has to work as well&views=6

POST : edit a post
http://localhost:8000/api/v1/posts/edit/3?title=last Test Edited&body=Edited Has to work well&views=34

POST : Delete a post
http://localhost:8000/api/v1/posts/delete/1

POST : add user
http://localhost:8000/api/v1/users/add?username=Melchisedec&email=afrod.m@affroapps.com&password=secret


GET : all users
http://localhost:8000/api/v1/users/