<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group([ 'prefix' => 'api/v1' ], function( $router )
{

	// posts
	//protects all routes from the routes file
	// $router->group([ 'prefix' => 'posts', 'middleware' => 'auth' ], function( $router ){
	$router->group([ 'prefix' => 'posts' ], function( $router ){
		// applying middleware to specific routes
	    $router->group( [ 'middleware' => 'auth' ], function( $router ){
	    	$router->get( '/', 'PostsController@index' );
	    	$router->post( 'add', 'PostsController@createPost' );
	    });
		
		
		$router->get( 'view/{id}', 'PostsController@viewPost' );
		$router->post( 'edit/{id}', 'PostsController@updatePost' );
		$router->post( 'delete/{id}', 'PostsController@deletePost' );
	});

	// users
	$router->group([ 'prefix' => 'users', 'middleware' => 'cors' ], function( $router ){



		$router->get( '/', 'UsersController@index' );
		$router->post( 'add', 'UsersController@createUser' );
		// $router->put( 'add', 'UsersController@createUser' );
		$router->get( 'view/{id}', 'UsersController@viewUser' );
		$router->post( 'edit/{id}', 'UsersController@updateUser' );
		$router->post( 'delete/{id}', 'UsersController@deleteUser' );
	});
	
});




