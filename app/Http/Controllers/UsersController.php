<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Hashing\BcryptHasher;

class UsersController extends Controller
{


    // Create new post
    public function createUser( Request $request )
    {
        $request[ 'api_token' ] = str_random( 60 );
        $request[ 'password' ] = app( 'hash' )->make( $request[ 'password' ] );
        $user = User::create( $request->all() );

        return response()->json( $user );
    }

    // Update post
    public function updateUser( Request $request, $id )
    {
        $user = User::find( $id );
        $post->username = $request->input( 'username' );
        $post->email = $request->input( 'email' );
        $post->password = $request->input( 'password' );
        $post->api_token = $request->input( 'api_token' );
        $post->save();

        return response()->json( $post );
    }

    // Delete post
    public function deleteUser( $id )
    {
        $post = Post::find( $id );
        $post->delete();

        return response()->json( 'Removed successfully' );
    }

    // List posts
    public function index()
    {
        $users = User::all();

        return response()->json( $users );
    }

    // Get Post
    public function viewUser( $id )
    {
        $post = Post::find( $id );

        return response()->json( $post );
        //echo 'me';
    }
}
